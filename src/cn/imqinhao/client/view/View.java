package cn.imqinhao.client.view;

import cn.imqinhao.client.service.MessageService;
import cn.imqinhao.client.service.UserService;
import cn.imqinhao.client.utils.Utility;
import cn.imqinhao.common.User;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * 客户端菜单界面
 */
public class View {
    // 控制是否显示菜单
    private boolean loop = true;
    // 接收用户的键盘输入
    private String key = "";
    // 用于登录服务器/注册
    private UserService userService = new UserService();
    private MessageService messageService = new MessageService();

    public static void main(String[] args) {
        new View().mainMenu();
    }

    public static boolean isSpecialChar(String str) {
        String regEx = "[ _`~!@#$%^&*()+=|{}':;',\\[\\].<>/?~！@#￥%……&*（）——+|{}【】‘；：”“’。，、？]|\n|\r|\t";
        Pattern p = Pattern.compile(regEx);
        Matcher m = p.matcher(str);
        return m.find();
    }

    // 显示主菜单
    private void mainMenu() {
        while (loop) {
            System.out.println("****************** 欢迎使用大佬们的聊天室 *****************");
            System.out.println("*                     1. 用户注册                      *");
            System.out.println("*                     2. 用户登录                      *");
            System.out.println("*                     3. 找回密码                      *");
            System.out.println("*                     4. 退出系统                      *");
            System.out.println("******************************************************");
            System.out.print("请输入您的选择：");

            key = Utility.readString(1);
            // 根据用户的输入处理不同的逻辑
            switch (key) {
                case "1":
                    System.out.println("----------------------- 用户注册 -----------------------");
                    System.out.print("请输入用户名：");
                    String username = Utility.readString(20);
                    System.out.println("请选择密保问题：");
                    System.out.println("1. 你喜欢看的电影?");
                    System.out.println("2. 你毕业于那个初中？");
                    System.out.println("3. 你最喜欢的城市？");
                    System.out.println("4. 你爱人的姓名？");
                    System.out.println("5. 你爸爸是谁？");
                    System.out.print("请输入您的选择（序号）：");
                    int questionId = Utility.readInt();
                    System.out.print("请输入答案：");
                    String answer = Utility.readString(30);
                    System.out.print("请输入您的邮箱（请填写真实邮件，您的大佬ID将发送至您的邮箱）：");
                    String email = Utility.readString(30);
                    System.out.print("请输入密码：");
                    String password = Utility.readString(16);
                    boolean hasSpecialChar = isSpecialChar(password);
                    while (hasSpecialChar) {
                        System.out.println("密码不允许包含特殊字符");
                        System.out.print("请输入密码：");
                        password = Utility.readString(16);
                    }
                    System.out.print("请再次输入密码：");
                    String passwordAgain = Utility.readString(16);
                    while (!passwordAgain.equals(password)) {
                        System.out.print("请输入密码：");
                        password = Utility.readString(16);
                        System.out.print("请再次输入密码：");
                        passwordAgain = Utility.readString(16);
                    }
                    // 创建User对象
                    User user = new User();
                    user.setUsername(username);
                    user.setQuestionId(questionId);
                    user.setAnswer(answer);
                    user.setEmail(email);
                    user.setPassword(passwordAgain);
                    // 发送给服务器提交判断是否注册成功
                    userService.Register(user);
//                    if (true) {
//
//                    } else {
//
//                    }
                    break;
                case "2":
                    System.out.println("----------------------- 用户登录 -----------------------");
                    System.out.print("请输入账号：");
                    int loginUid = Utility.readInt();
                    System.out.print("请输入密码：");
                    String loginPassword = Utility.readString(16);
                    if (userService.checkUser(loginUid, loginPassword)) {
                        System.out.println("\n******************* 欢迎用户【" + loginUid + "】********************\n");
                        // 进入二级菜单
                        while (loop) {
                            System.out.println("***********************【" + loginUid + "】***********************");
                            System.out.println("-                  1.查看在线人员名单                    -");
                            System.out.println("-                  2.私         聊                    -");
                            System.out.println("-                  3.群         聊                    -");
                            System.out.println("-                  4.退         出                    -");
                            System.out.println("-                  5.账  号  注  销                    -");
                            System.out.println("-                  6.修  改  密  码                    -");
                            System.out.println("******************************************************");
                            System.out.print("请输入您的选择：");
                            key = Utility.readString(1);
                            switch (key) {
                                case "1":
                                    userService.onlineFriendList();
                                    try {
                                        Thread.sleep(1000);
                                    } catch (InterruptedException e) {
                                        e.printStackTrace();
                                    }
                                    System.out.println();
                                    break;
                                case "2":
                                    System.out.print("请输入想要聊天的用户号(在线): ");
                                    int getter = Utility.readInt();
                                    System.out.print("请输入想说的话: ");
                                    String content = Utility.readString(100);
                                    messageService.sendMessageToOne(content, loginUid, getter);
                                    break;
                                case "3":
                                    System.out.print("请输入想对大家说的话：");
                                    String content1 = Utility.readString(100);
                                    messageService.sendMessageToAll(content1, loginUid);
                                    break;
                                case "4":
                                    // 调用方法，给服务器发送一个退出系统的Message
                                    loop = false;
                                    userService.exit();
                                    break;
                                case "5":
                                    System.out.println("您确定要注销您的账号吗，注销后您的大佬ID将会被回收，资料也将被清空哦");
                                    System.out.print("请输入您的选择（Y/N)：");
                                    String choice;
                                    do {
                                        choice = Utility.readString(1);
                                    } while (!choice.equalsIgnoreCase("Y") && !choice.equalsIgnoreCase("N"));
                                    if (choice.equalsIgnoreCase("Y")) {
                                        userService.logOff();
                                        try {
                                            Thread.sleep(5000);
                                        } catch (InterruptedException e) {
                                            e.printStackTrace();
                                        }
                                        userService.exit();
                                    }
                                    break;
                                case "6":
                                    System.out.print("请输入您想要修改的密码：");
                                    String pwd = Utility.readString(16);
                                    String pwdAgain;
                                    do {
                                        System.out.print("请再次输入密码：");
                                        pwdAgain = Utility.readString(16);
                                    } while (!pwd.equals(pwdAgain));
                                    userService.modifyPassword(pwd);
                                    try {
                                        Thread.sleep(5000);
                                    } catch (InterruptedException e) {
                                        e.printStackTrace();
                                    }
                                    break;
                            }
                        }
                    } else {
                        // 登录失败
                        System.out.println("用户名或密码错误，请检查！\n");
                    }
                    break;
                case "3":
                    System.out.println("----------------------- 找回密码 -----------------------");
                    System.out.print("请输入您要找回密码的账号：");
                    int userId = Utility.readInt();
                    System.out.print("请输入您绑定的邮箱：");
                    String emailAddress = Utility.readString(30);
                    // TODO 验证邮件的正确性
                    System.out.print("请输入您邮件收到的验证码: ");
                    userService.getCode(emailAddress);
                    String checkCode = Utility.readString(4);
                    break;
                case "4":
                    System.out.println("退出系统");
                    loop = false;
                    break;
            }
        }
    }
}
