package cn.imqinhao.client.service;

import java.util.HashMap;

/**
 * @author tttttt
 * @version 1.0
 * 管理客户端到服务器端的线程
 */
public class ManageClientConnectServerThread {
    /**
     * key 用户id
     * value 线程
     */
    private static HashMap<Integer, ClientConnectServerThread> hashMap = new HashMap<>();

    // 将线程加入到集合中
    public static void addClientConnectServerThread(Integer userID, ClientConnectServerThread clientConnectServerThread) {
        hashMap.put(userID, clientConnectServerThread);
    }

    public static ClientConnectServerThread getClientConnectServerThread(Integer userId) {
        return hashMap.get(userId);
    }

}
