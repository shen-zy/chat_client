package cn.imqinhao.client.service;

import cn.imqinhao.common.Message;
import cn.imqinhao.common.MessageType;

import java.io.IOException;
import java.io.ObjectOutputStream;
import java.util.Date;

/**
 * @author tttttt
 * @version 1.0
 * 提供和消息相关的服务方法
 */
public class MessageService {
    /**
     * 私聊功能
     * @param content 消息内容
     * @param senderId 发送用户ID
     * @param getterId 接收用户ID
     */
    public void sendMessageToOne(String content, Integer senderId, Integer getterId) {
        Message message = new Message();
        message.setSender(senderId);
        message.setGetter(getterId);
        message.setContent(content);
        message.setSendTime(new Date().toString()); // 发送时间
        message.setMessageType(MessageType.MESSAGE_COMMON_MES);
        System.out.println(senderId + "【我】 对 " + getterId + " 说 " + content);
        // 发送给服务端
        try {
            ObjectOutputStream objectOutputStream = new ObjectOutputStream(ManageClientConnectServerThread.getClientConnectServerThread(senderId).getSocket().getOutputStream());
            objectOutputStream.writeObject(message);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * 群发消息
     * @param content 群发内容
     * @param senderId 发送者ID
     */
    public void sendMessageToAll(String content, Integer senderId) {
        Message message = new Message();
        message.setMessageType(MessageType.MESSAGE_TO_ALL_MES);
        message.setContent(content);
        message.setSender(senderId);
        message.setSendTime(new Date().toString());
        System.out.println(senderId + "【我】 对 【所有人】 说: " + content);
        // 发送给服务端
        try {
            ObjectOutputStream objectOutputStream = new ObjectOutputStream(ManageClientConnectServerThread.getClientConnectServerThread(senderId).getSocket().getOutputStream());
            objectOutputStream.writeObject(message);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
