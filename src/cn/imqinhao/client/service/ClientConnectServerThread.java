package cn.imqinhao.client.service;

import cn.imqinhao.common.Message;
import cn.imqinhao.common.MessageType;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.net.Socket;

/**
 * @author tttttt
 * @version 1.0
 */
public class ClientConnectServerThread extends Thread {
    // 该线程需要持有socket
    private Socket socket;

    public ClientConnectServerThread() {
    }

    public ClientConnectServerThread(Socket socket) {
        this.socket = socket;
    }

    public Socket getSocket() {
        return socket;
    }

    @Override
    public void run() {
        ObjectInputStream objectInputStream = null;
        while (true) {
            try {
//                System.out.println("\n客户端线程，等待读取从服务器端发送的消息");
                objectInputStream = new ObjectInputStream(socket.getInputStream());
                Message message = (Message) objectInputStream.readObject();
                // 读取到的数据是 服务器返回的用户列表
                if (message.getMessageType().equals(MessageType.MESSAGE_RETURN_ONLINE_FRIEND)) {
                    // 取出在线用户列表，并展示
                    String[] onlineUsers = message.getContent().split(" ");
                    System.out.println("\n--------------------- 当前在线用户列表 ---------------------");
                    for (int i = 0; i < onlineUsers.length; i++) {
                        System.out.println("【 " + i + " 】  " + onlineUsers[i]);
                    }
                }
                // 读取到数据是 普通的聊天记录
                else if (message.getMessageType().equals(MessageType.MESSAGE_COMMON_MES)) {
                    // 把服务器转发的消息显示到控制台即可
                    System.out.println("\n" + message.getSender() + " 对 " + message.getGetter() + "【我】 说： " + message.getContent());
                }
                // 读取到的数据是群聊消息
                else if (message.getMessageType().equals(MessageType.MESSAGE_TO_ALL_MES)) {
                    System.out.println("\n" + message.getSender() + " 对 【所有人】 说: " + message.getContent());
                }
                // 读取到的是返回用户注销是否成功的消息
                else if (message.getMessageType().equals(MessageType.MESSAGE_RETURN_LOGOFF_MES)) {
                    System.out.println(message.getGetter() + "，您的账号 " + message.getContent());
                    System.out.println("您的账号将在5秒钟后强制下线！");
                }
                else if (message.getMessageType().equals(MessageType.MESSAGE_RETURN_MODIFY_IS_OK_MES)) {
                    System.out.println(message.getGetter() + " ，您的 " + message.getContent());
                    System.out.println("您的新密码将在下次重新登录时生效！");
                }else{
                    System.out.println(message.getContent());
                }
            } catch (Exception e) {
                try {
                    objectInputStream.close();
                } catch (IOException ex) {
                    ex.printStackTrace();
                } finally {
                    System.exit(0);
                }
//                e.printStackTrace();
            }
        }
    }
}
