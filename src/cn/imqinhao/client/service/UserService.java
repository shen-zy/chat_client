package cn.imqinhao.client.service;

import cn.imqinhao.common.Message;
import cn.imqinhao.common.MessageType;
import cn.imqinhao.common.User;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.InetAddress;
import java.net.Socket;

/**
 * @author tttttt
 * @version 1.0
 */
public class UserService {
    private User loginUser = new User();
    private Socket socket;
    private ObjectInputStream objectInputStream = null;
    private static final String IP = "10.40.226.117";

    /**
     * 根据 大佬号 和 password 到服务器验证该用户是否合法
     *
     * @param uid      大佬号
     * @param password 密码
     * @return 用户是否合法
     */
    public boolean checkUser(int uid, String password) {
        // 登录是否成功
        boolean isSuccess = false;

        loginUser.setUID(uid);
        loginUser.setPassword(password);

        try {
            // 连接服务端，发送user对象
            socket = new Socket(IP, 29898);
            ObjectOutputStream objectOutputStream = new ObjectOutputStream(socket.getOutputStream());
            // 发送loginUser对象
            Message messages = new Message();
            messages.setUser(loginUser);
            messages.setMessageType(MessageType.MESSAGE_USER_LOGIN_MES);
            objectOutputStream.writeObject(messages);
            // 读取服务器回复的Message对象
            objectInputStream = new ObjectInputStream(socket.getInputStream());
            Message message = (Message) objectInputStream.readObject();
            // 服务器响应类型
            if (message.getMessageType().equals(MessageType.MESSAGE_LOGIN_SUCCEED)) {
                // 登录成功
                // 创建一个和服务器保持通信的线程
                ClientConnectServerThread clientConnectServerThread = new ClientConnectServerThread(socket);
                // 启动客户端线程
                clientConnectServerThread.start();
                // 考虑到可扩展性，将线程放入到集合集中管理
                ManageClientConnectServerThread.addClientConnectServerThread(uid, clientConnectServerThread);
                isSuccess = true;
            } else {
                // 登录失败
                // 断开与服务器的通信
                socket.close();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return isSuccess;
    }

    /**
     * 用户注册
     * @param user 注册的用户对象
     */
    public void Register(User user) {
        try {
            socket = new Socket(IP, 29898);
            Message message = new Message();
            message.setMessageType(MessageType.MESSAGE_USER_REGISTER_MES);
            message.setUser(user);
            ObjectOutputStream objectOutputStream = new ObjectOutputStream(socket.getOutputStream());
            objectOutputStream.writeObject(message);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * 检查邮箱是否正确
     * @param userId 用户ID
     * @param email 邮箱
     * @return 邮箱是否正确
     */
    public boolean checkEmail(Integer userId, String email) {
        Message message1 = new Message();
        message1.setMessageType(MessageType.MESSAGE_CHECK_EMAIL_MES);
        message1.setSender(userId);
        message1.setContent(email);
        try {
            socket = new Socket(IP, 29898);
            ObjectOutputStream objectOutputStream = new ObjectOutputStream(socket.getOutputStream());
            objectOutputStream.writeObject(message1);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return false;
    }

    /**
     * 申请验证码
     * @param email 申请验证码的邮箱
     */
    public void getCode(String email) {
        Message message = new Message();
        message.setMessageType(MessageType.MESSAGE_GET_CODE_MES);
        message.setContent(email);
        try {
            socket = new Socket(InetAddress.getLocalHost(), 29898);
            ObjectOutputStream objectOutputStream = new ObjectOutputStream(socket.getOutputStream());
            objectOutputStream.writeObject(message);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * 向服务器请求在线用户列表
     */
    public void onlineFriendList() {
        Message message = new Message();
        message.setMessageType(MessageType.MESSAGE_GET_ONLINE_FRIEND);
        message.setSender(loginUser.getUID());
        // 发送请求
        try {
            ObjectOutputStream objectOutputStream = new ObjectOutputStream(ManageClientConnectServerThread.getClientConnectServerThread(loginUser.getUID()).getSocket().getOutputStream());
            // 发送一个Message，请求在线列表
            objectOutputStream.writeObject(message);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * 退出系统，给服务器发送退出系统的message
     */
    public void exit() {
        Message message = new Message();
        message.setMessageType(MessageType.MESSAGE_CLIENT_EXIT);
        // 指定我是哪个客户端id
        message.setSender(loginUser.getUID());

        try {
            // 发送message
            ObjectOutputStream objectOutputStream = new ObjectOutputStream(ManageClientConnectServerThread.getClientConnectServerThread(loginUser.getUID()).getSocket().getOutputStream());
            objectOutputStream.writeObject(message);
            System.out.println(loginUser.getUID() + " 退出了系统");
            // 结束进程
            System.exit(0);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * 用户申请注销账户
     */
    public void logOff() {
        Message message = new Message();
        message.setMessageType(MessageType.MESSAGE_LOGOFF_MES);
        message.setSender(loginUser.getUID());

        try {
            ObjectOutputStream objectOutputStream = new ObjectOutputStream(ManageClientConnectServerThread.getClientConnectServerThread(loginUser.getUID()).getSocket().getOutputStream());
            objectOutputStream.writeObject(message);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * 用户修改密码
     *
     * @param password 新密码
     */
    public void modifyPassword(String password) {
        Message message = new Message();
        message.setMessageType(MessageType.MESSAGE_MODIFY_PASSWORD_MES);
        message.setContent(password);
        message.setSender(loginUser.getUID());
        try {
            // 发送数据
            ObjectOutputStream objectOutputStream = new ObjectOutputStream(ManageClientConnectServerThread.getClientConnectServerThread(loginUser.getUID()).getSocket().getOutputStream());
            objectOutputStream.writeObject(message);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
