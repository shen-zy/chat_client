package cn.imqinhao.common;

import java.io.Serializable;

/**
 * @author tttttt
 * @version 1.0
 */
public class LoginUser implements Serializable {
    private static final long serialVersionUID = 1L;
    private int uid;
    private String password;

    public LoginUser() {
    }

    public LoginUser(int uid, String password) {
        this.uid = uid;
        this.password = password;
    }

    public int getUid() {
        return uid;
    }

    public void setUid(int uid) {
        this.uid = uid;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}
