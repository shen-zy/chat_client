package cn.imqinhao.common;

public interface MessageType {
    String MESSAGE_LOGIN_SUCCEED = "1"; // 表示登录成功
    String MESSAGE_LOGIN_FAILED = "2";  // 表示登录失败
    String MESSAGE_COMMON_MES = "3";    // 表示普通信息包
    String MESSAGE_GET_ONLINE_FRIEND = "4";  // 要求返回在线用户列表
    String MESSAGE_RETURN_ONLINE_FRIEND = "5";  // 返回在线用户列表
    String MESSAGE_CLIENT_EXIT = "6";   // 客户端请求退出
    String MESSAGE_TO_ALL_MES = "7";    // 表示群发消息
    String MESSAGE_LOGOFF_MES = "8";    // 要求注销用户
    String MESSAGE_RETURN_LOGOFF_MES = "9"; // 返回用户注销是否成功
    String MESSAGE_MODIFY_PASSWORD_MES = "10";  // 用户请求修改密码
    String MESSAGE_RETURN_MODIFY_IS_OK_MES = "11";  // 密码修改是否成功
    String MESSAGE_USER_REGISTER_MES = "12";    // 用户注册请求
    String MESSAGE_RETURN_USER_REGISTER_MES = "13"; // 服务器返回用户是否注册成功
    String MESSAGE_USER_LOGIN_MES = "14";   // 用户登录请求
    String MESSAGE_RETURN_USER_LOGIN_MES = "15";    // 返回用户登录是否成功
    String MESSAGE_CHECK_EMAIL_MES = "16";  // 请求服务器验证邮箱是否正确
    String MESSAGE_RETURN_CHECK_EMAIL_MES = "17";   // 返回邮箱是否正确
    String MESSAGE_GET_CODE_MES = "18"; // 用户请求获取验证码
    String MESSAGE_SEND_CODE_MES = "19"; // 用户发送验证码申请服务器验证
}
