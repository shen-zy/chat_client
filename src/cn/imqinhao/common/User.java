package cn.imqinhao.common;

import java.io.Serializable;

/**
 * 表示一个用户信息
 */
public class User implements Serializable {

    private static final long serialVersionUID = 1L;

    private int UID;    // 用户ID
    private String username;    // 用户名
    private String password;    // 密码
    private int questionId;     // 问题ID
    private String answer;      // 密保答案
    private String email;   // 邮箱

    public int getUID() {
        return UID;
    }

    public void setUID(int UID) {
        this.UID = UID;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public int getQuestionId() {
        return questionId;
    }

    public void setQuestionId(int questionId) {
        this.questionId = questionId;
    }

    public String getAnswer() {
        return answer;
    }

    public void setAnswer(String answer) {
        this.answer = answer;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    @Override
    public String toString() {
        return "User{" +
                "UID=" + UID +
                ", username='" + username + '\'' +
                ", password='" + password + '\'' +
                ", questionId=" + questionId +
                ", answer='" + answer + '\'' +
                ", email='" + email + '\'' +
                '}';
    }
}
